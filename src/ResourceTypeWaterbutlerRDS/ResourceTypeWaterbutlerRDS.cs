﻿using Amazon.S3;
using Amazon.S3.Model;
using Coscine.Configuration;
using Coscine.ECSManager;
using Coscine.ResourceTypeBase;
using Coscine.WaterbutlerHelper;
using Coscine.WaterbutlerHelper.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Coscine.ResourceTypeWaterbutlerRDS
{
    public class ResourceTypeWaterbutlerRDS : ResourceTypeDefinition
    {
        private readonly string _rdsKeyPrefix;

        private readonly WaterbutlerInterface _waterbutlerInterface;

        private readonly EcsManager _ecsManager;

        public ResourceTypeWaterbutlerRDS(string name, IConfiguration gConfig, ResourceTypeConfigurationObject resourceTypeConfiguration) : base(name, gConfig, resourceTypeConfiguration)
        {
            _rdsKeyPrefix = resourceTypeConfiguration.Config["rdsKey"];
            _waterbutlerInterface = new WaterbutlerInterface(Configuration, new DataSourceService(new HttpClient()));
            _ecsManager = new EcsManager
            {
                EcsManagerConfiguration = new EcsManagerConfiguration
                {
                    ManagerApiEndpoint = Configuration.GetString($"{_rdsKeyPrefix}/manager_api_endpoint"),
                    NamespaceName = Configuration.GetString($"{_rdsKeyPrefix}/namespace_name"),
                    NamespaceAdminName = Configuration.GetString($"{_rdsKeyPrefix}/namespace_admin_name"),
                    NamespaceAdminPassword = Configuration.GetString($"{_rdsKeyPrefix}/namespace_admin_password"),
                    ObjectUserName = Configuration.GetString($"{_rdsKeyPrefix}/object_user_name"),
                    ReplicationGroupId = Configuration.GetString($"{_rdsKeyPrefix}/replication_group_id"),
                }
            };
        }

        public override async Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(prefix, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{prefix}\".");
            }

            var entries = new List<ResourceEntry>();

            foreach (var info in infos)
            {
                entries.Add(new ResourceEntry(info.Path, info.IsFile, info.Size == null ? 0 : (long)info.Size, null, null, info.Created == null ? new DateTime() : (DateTime)info.Created, info.Modified == null ? new DateTime() : (DateTime)info.Modified));
            }

            return entries;
        }

        public override async Task<ResourceEntry> GetEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{key}\".");
            }

            // Not a file
            if (infos.Count > 1 || !infos[0].IsFile)
            {
                throw new Exception("Not a file.");
            }

            return new ResourceEntry(key, infos[0].IsFile, (long)infos[0].Size, null, null, infos[0].Created == null ? new DateTime() : (DateTime)infos[0].Created, infos[0].Modified == null ? new DateTime() : (DateTime)infos[0].Modified);
        }

        public override async Task StoreEntry(string id, string key, Stream body, Dictionary<string, string> options = null)
        {
            var url = await GetEntryStoreUrl(key.TrimStart('/'), null, options);

            HandleResponse(UploadObject(url, body));
        }

        private static HttpWebResponse UploadObject(Uri url, Stream stream)
        {
            HttpWebRequest httpRequest = WebRequest.Create(url) as HttpWebRequest;
            httpRequest.Method = "PUT";
            using (Stream dataStream = httpRequest.GetRequestStream())
            {
                var buffer = new byte[8000];
                int bytesRead = 0;
                while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    dataStream.Write(buffer, 0, bytesRead);
                }
            }
            return httpRequest.GetResponse() as HttpWebResponse;
        }

        public override async Task DeleteEntry(string id, string key, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under {key}.");
            }

            // Not a file
            if (infos.Count > 1 || !infos[0].IsFile)
            {
                throw new Exception("Not a file.");
            }

            HandleResponse(await _waterbutlerInterface.DeleteObjectAsync(infos[0], authHeader));
        }

        public override async Task<Stream> LoadEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);

            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{key}\".");
            }

            // Not a file
            if (infos.Count > 1 || !infos[0].IsFile)
            {
                throw new Exception("Not a file.");
            }

            // Do not dispose the response!
            // The stream is later accessed by the MVC, to deliver the file.
            // When disposed, the stream becomes invalid and can't be read.
            var response = await _waterbutlerInterface.DownloadObjectAsync(infos[0], authHeader);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStreamAsync();
            }

            return null;
        }

        protected static void HandleResponse(HttpResponseMessage httpResponseMessage)
        {
            var statuscode = (int)httpResponseMessage.StatusCode;
            if (statuscode == 403)
            {
                throw new RTDForbiddenException();
            }
            else if (statuscode == 404)
            {
                throw new RTDNotFoundException();
            }
            else if (statuscode >= 400 && statuscode < 600)
            {
                throw new RTDBadRequestException();
            }
        }

        protected static void HandleResponse(HttpWebResponse httpWebResponse)
        {
            var statuscode = (int)httpWebResponse.StatusCode;
            if (statuscode == 403)
            {
                throw new RTDForbiddenException();
            }
            else if (statuscode == 404)
            {
                throw new RTDNotFoundException();
            }
            else if (statuscode >= 400 && statuscode < 600)
            {
                throw new RTDBadRequestException();
            }
        }

        public override async Task CreateResource(Dictionary<string, string> options = null)
        {
            await _ecsManager.CreateBucket(options["bucketname"], int.Parse(options["size"]));
        }

        public override async Task<long> GetResourceQuotaUsed(string id, Dictionary<string, string> options = null)
        {
            var amazonConfig = new AmazonS3Config
            {
                ServiceURL = options["endpoint"],
                ForcePathStyle = true
            };

            using var client = new AmazonS3Client(options["accessKey"], options["secretKey"], amazonConfig);
            long totalFileSize = 0;
            long fileCount = 0;
            var listRequest = new ListObjectsRequest()
            {
                BucketName = options["bucketname"]
            };

            ListObjectsResponse listResponse;
            do
            {
                listResponse = await client.ListObjectsAsync(listRequest);
                fileCount += listResponse.S3Objects.Count;
                totalFileSize += listResponse.S3Objects.Sum(x => x.Size);
                listRequest.Marker = listResponse.NextMarker;
            } while (listResponse.IsTruncated);

            return totalFileSize;
        }

        public override async Task<long> GetResourceQuotaAvailable(string id, Dictionary<string, string> options = null)
        {
            return await _ecsManager.GetBucketQuota(options["bucketname"]);
        }

        public override async Task SetResourceQuota(string id, int quota, Dictionary<string, string> options = null)
        {
            await _ecsManager.SetBucketQuota(options["bucketname"], quota);
        }

        public override async Task<Uri> GetEntryDownloadUrl(string key, string version = null, Dictionary<string, string> options = null)
        {
            return await Task.Run(() =>
            {
                var amazonConfig = new AmazonS3Config
                {
                    ServiceURL = options["endpoint"],
                    ForcePathStyle = true
                };

                using var client = new AmazonS3Client(options["accessKey"], options["secretKey"], amazonConfig);
                var presignedUrl = client.GetPreSignedURL(new GetPreSignedUrlRequest()
                {
                    BucketName = options["bucketname"],
                    Key = key,
                    Verb = HttpVerb.GET,
                    // The Simulator uses HTTP and the live system HTTPS
                    Protocol = options["endpoint"].Contains("ecs-sim01.itc.rwth-aachen.de") ? Protocol.HTTP : Protocol.HTTPS,
                    // For now, expiry of a day is set, but this might be up to debate
                    Expires = DateTime.UtcNow.AddHours(24)
                });
                return new Uri(presignedUrl);
            });
        }

        public override async Task<Uri> GetEntryStoreUrl(string key, string version = null, Dictionary<string, string> options = null)
        {
            return await Task.Run(() =>
            {
                var amazonConfig = new AmazonS3Config
                {
                    ServiceURL = options["endpoint"],
                    ForcePathStyle = true
                };

                using var client = new AmazonS3Client(options["accessKey"], options["secretKey"], amazonConfig);
                var presignedUrl = client.GetPreSignedURL(new GetPreSignedUrlRequest()
                {
                    BucketName = options["bucketname"],
                    Key = key,
                    Verb = HttpVerb.PUT,
                    // The Simulator uses HTTP and the live system HTTPS
                    Protocol = options["endpoint"].Contains("ecs-sim01.itc.rwth-aachen.de") ? Protocol.HTTP : Protocol.HTTPS,
                    // For now, expiry of a day is set, but this might be up to debate
                    Expires = DateTime.UtcNow.AddHours(24)
                });
                return new Uri(presignedUrl);
            });
        }

        public override async Task<ResourceTypeInformation> GetResourceTypeInformation()
        {
            var resourceTypeInformation = await base.GetResourceTypeInformation();
            resourceTypeInformation.IsQuotaAvailable = true;
            resourceTypeInformation.IsQuotaAdjustable = true;
            return await Task.FromResult(resourceTypeInformation);
        }
    }
}